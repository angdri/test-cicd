package com.example.janken31.view.menu.fragment.history

import com.example.janken31.model.api.battle.response.GetBattleData
import com.example.janken31.view.ApiView

interface HistoryView : ApiView {
    fun showLoading(visibility : Int)
    fun historyIsEmpty()
    fun showHistory(result : MutableList<GetBattleData>)
    fun showMessage(msg: String)
}