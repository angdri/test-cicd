package com.example.janken31.view.battle.vsPlayer

import com.example.janken31.view.ApiView
import com.example.janken31.view.battle.BattleView

interface VsPlayerView: BattleView, ApiView {
    fun setWinner(result: String)
    fun showError(error: String)
}