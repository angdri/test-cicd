package com.example.janken31.view.saveBattle

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.janken31.R
import com.example.janken31.model.room.History
import kotlinx.android.synthetic.main.item_history.view.*

class SaveBattleAdapter : RecyclerView.Adapter<SaveBattleAdapter.ViewHolder>(){
    private val listHistory = mutableListOf<History>()
    private lateinit var listenerHistory: OnClickListenerHistory

    fun setOnClickListener(listenerHistory: OnClickListenerHistory){
        this.listenerHistory = listenerHistory
    }

    inner class ViewHolder(val view: View): RecyclerView.ViewHolder(view){
        fun bind(history: History, position: Int){
            view.tvHistoryTanggalForm.text = history.date
            view.tvHistoryModeForm.text = history.battleMode
            view.tvHistoryHasilForm.text = history.result
            listenerHistory.let{
                view.btnDeleteHistory.setOnClickListener {
                    listenerHistory.deleteHistory(history,position)
                }
            }
        }
    }

    fun addData(listHistory:List<History>){
        this.listHistory.clear()
        this.listHistory.addAll(listHistory)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_history, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = listHistory.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(listHistory[position], position)
    }

    interface OnClickListenerHistory{
        fun deleteHistory(history: History, position: Int)
    }
}