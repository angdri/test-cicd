package com.example.janken31.view.landingPage

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.viewpager.widget.ViewPager
import com.example.janken31.R
import com.example.janken31.utils.LANDING_PAGE
import com.example.janken31.view.login.LoginActivity
import kotlinx.android.synthetic.main.activity_landing_page.*

class LandingPageActivity : AppCompatActivity(), LandingPageView {
    private lateinit var adapterViewPager: AdapterViewPager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_landing_page)

        adapterViewPager = AdapterViewPager(supportFragmentManager)
        viewPager.adapter = adapterViewPager

        dots_indicator.setViewPager(viewPager)

        nextButton.setOnClickListener {
            val intent = Intent(this, LoginActivity::class.java)
            finish()
            startActivity(intent)
            Log.d(LANDING_PAGE, "Go to LoginActivity")
        }

        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener{
            override fun onPageScrollStateChanged(state: Int) {}

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {}

            override fun onPageSelected(position: Int) {
                if(position == adapterViewPager.count.minus(1)){
                    showNextButton(true)
                }else{
                    showNextButton(false)
                }
            }

        })
    }

    override fun showNextButton(bool: Boolean) {
        if (bool) {
            nextButton.visibility = View.VISIBLE
        } else {
            nextButton.visibility = View.GONE
        }
    }
}
