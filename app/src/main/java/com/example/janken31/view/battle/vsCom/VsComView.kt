package com.example.janken31.view.battle.vsCom

import com.example.janken31.view.ApiView
import com.example.janken31.view.battle.BattleView

interface VsComView: BattleView, ApiView {
    fun setWinner(result: String)
    fun showError(error: String)
    fun setComChoiceBackground(choice: Int)
}