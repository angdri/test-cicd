package com.example.janken31.view.menu.fragment.history

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.janken31.R
import com.example.janken31.model.api.battle.response.GetBattleData
import com.example.janken31.presenter.HistoryPresenter
import com.example.janken31.utils.PreferenceHelper
import com.example.janken31.view.login.LoginActivity
import com.google.android.material.snackbar.Snackbar
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_history.*
import javax.inject.Inject


class HistoryFragment : DaggerFragment(), HistoryView {

    private lateinit var adapter: HistoryAdapter
    //Dagger
    @Inject
    lateinit var presenter: HistoryPresenter
    @Inject
    lateinit var preferenceHelper: PreferenceHelper

    //Koin
//    private val preferenceHelper by inject<PreferenceHelper>()
//    private val presenter by inject<HistoryPresenter>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(false)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_history, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        preferenceHelper = PreferenceHelper(context!!)
        presenter.setView(this)
        adapter = HistoryAdapter()
        presenter.getHistory(preferenceHelper.token!!)
        rvHistory.layoutManager = LinearLayoutManager(context)
        rvHistory.adapter = adapter
    }

    override fun showLoading(visibility: Int) {
        pbHistory.visibility = visibility
        if (visibility == View.GONE) {
            contentHistoryFragment.visibility = View.VISIBLE
        }else{
            contentHistoryFragment.visibility = View.GONE
        }
    }

    override fun historyIsEmpty() {
        rvHistory.visibility = View.GONE
        tvEmptyHistory.visibility = View.VISIBLE
    }

    override fun showHistory(result: MutableList<GetBattleData>) {
        rvHistory.visibility = View.VISIBLE
        tvEmptyHistory.visibility = View.GONE
        adapter.addData(result)
    }

    override fun showMessage(msg: String) {
        val snackbar = Snackbar.make(clHistoryFragment, msg, Snackbar.LENGTH_LONG).apply {
            setTextColor(Color.RED)
            view.setBackgroundColor(Color.WHITE)
        }
        snackbar.show()
    }

    override fun tokenUnauthorized() {
        preferenceHelper.token = ""
        preferenceHelper.username = ""
        preferenceHelper.userId = ""
        val intent = Intent(activity, LoginActivity::class.java).apply {
            putExtra("resultRegister", "Session anda telah berakhir, silahkan login kembali")
        }
        startActivity(intent)
        requireActivity().finish()
    }
}
