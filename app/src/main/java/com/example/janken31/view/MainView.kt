package com.example.janken31.view

interface MainView {
    fun showMessage(msg: String)
    fun isTokenValid(status : Boolean)
}