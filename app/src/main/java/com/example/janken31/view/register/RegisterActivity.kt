package com.example.janken31.view.register

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import com.example.janken31.R
import com.example.janken31.model.api.user.auth.RegisterAuth
import com.example.janken31.presenter.RegisterPresenter
import com.example.janken31.utils.DataRegexValidation.Companion.isEmailValid
import com.example.janken31.utils.DataRegexValidation.Companion.isUsernameValid
import com.example.janken31.view.login.LoginActivity
import com.google.android.material.snackbar.Snackbar
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_register.*
import javax.inject.Inject

class RegisterActivity : DaggerAppCompatActivity(), RegisterView {
    //Dagger
    @Inject
    lateinit var presenter: RegisterPresenter

    //Koin
//    private val presenter by inject<RegisterPresenter>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        presenter.setView(this)

        setSupportActionBar(findViewById(R.id.my_toolbar))
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = ""

        btnRegis.setOnClickListener {
            val username = etRegisterUsername.text.toString()
            val password = etRegisterPassword.text.toString()
            val email = etRegisterEmail.text.toString()

            if (username == "" && password == "" && email == "") {
                showMessage("Username, password, dan email tidak boleh kosong!")
            } else if (!isEmailValid(email)) {
                showMessage("Format email salah!")
            } else if (!isUsernameValid(username)){
                showMessage("Username hanya boleh mengandung angka dan huruf!")
            }else{
                val user = RegisterAuth(
                    email,
                    password,
                    username
                )
                presenter.registerUser(user)
            }
        }

        btnRegisterReset.setOnClickListener {
            reset()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun registerResult(result: Boolean) {
        if (result) {
            val intent = Intent(this, LoginActivity::class.java)
            intent.putExtra("resultRegister","Registrasi berhasil")
            startActivity(intent)
            finish()
        }
    }

    override fun showMessage(msg: String) {
        val snackbar = Snackbar.make(clRegisterActivity, msg, Snackbar.LENGTH_LONG)
        snackbar.setTextColor(Color.RED)
        snackbar.view.setBackgroundColor(Color.WHITE)
        snackbar.view.textAlignment = View.TEXT_ALIGNMENT_CENTER
        snackbar.show()
    }

    override fun reset() {
        etRegisterEmail.setText("")
        etRegisterPassword.setText("")
        etRegisterUsername.setText("")
    }

    override fun showLoading(visibility: Int) {
        pbRegister.visibility = visibility
        if (visibility == View.GONE) {
            contentRegisterActivity.visibility = View.VISIBLE
        }else{
            contentRegisterActivity.visibility = View.GONE
        }
    }
}
