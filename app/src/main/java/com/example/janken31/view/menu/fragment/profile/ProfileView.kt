package com.example.janken31.view.menu.fragment.profile

import android.widget.TextView
import com.example.janken31.view.ApiView

interface ProfileView: ApiView {
    fun updateData(username: String, email: String)
    fun setData(username: String, email:String)
    fun setPhoto(url: String)
    fun showEditDialog(title: String, data: String)
    fun setOnClickListener(viewTitle :TextView, viewForm:TextView, set:Boolean)
    fun showMessage(msg: String)
    fun showLoading(visibility : Int)
    fun getData(token : String)
    fun showConfirmDialog()
}