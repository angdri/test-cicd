package com.example.janken31.view.menu.fragment.battle

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.janken31.R
import com.example.janken31.utils.PreferenceHelper
import com.example.janken31.view.battle.vsCom.VsComActivity
import com.example.janken31.view.battle.vsPlayer.VsPlayerActivity
import com.example.janken31.view.menu.MenuView
import com.example.janken31.view.saveBattle.SaveBattleActivity
import kotlinx.android.synthetic.main.fragment_battle.*


class BattleFragment : Fragment() {
    private lateinit var listener: MenuView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_battle, container, false)

    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
        (activity as? AppCompatActivity)?.setSupportActionBar(activity?.findViewById(R.id.my_toolbar))
        (activity as? AppCompatActivity)?.supportActionBar?.title = ""

        val preferenceHelper = PreferenceHelper(activity?.baseContext!!)
        val username = preferenceHelper.username
        textViewVSPemain.text = "$username VS Pemain"
        textViewVSComp.text = "$username VS CPU"

        menuVSPemain.setOnClickListener {
            val intent = Intent(activity, VsPlayerActivity::class.java)
            startActivity(intent)
        }

        menuVSCPU.setOnClickListener {
            val intent = Intent(activity, VsComActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_app_bar, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.app_bar_save_battle -> {
                startActivity(Intent(activity, SaveBattleActivity::class.java))
                true
            }
            R.id.app_bar_keluar_aplikasi -> {
                requireActivity().finishAffinity()
                true
            }
            R.id.app_bar_logout -> {
                listener.logout()
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            this.listener = context as MenuView
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}
