package com.example.janken31.view.landingPage

interface LandingPageView{
    fun showNextButton(bool:Boolean)
}