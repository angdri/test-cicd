package com.example.janken31.view.login

import com.example.janken31.model.api.user.response.UserAuthResponse

interface LoginView {
    //API
    fun loginResult(status: Boolean, user: UserAuthResponse)

    fun reset()
    fun showMessage(data: String)
    fun showLoading(visibility : Int)
}