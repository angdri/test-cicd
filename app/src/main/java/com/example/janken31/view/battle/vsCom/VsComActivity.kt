package com.example.janken31.view.battle.vsCom

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import com.example.janken31.R
import com.example.janken31.presenter.VsComPresenter
import com.example.janken31.utils.Design
import com.example.janken31.utils.PreferenceHelper
import com.example.janken31.utils.VS_CPU_ACTIVITY
import com.example.janken31.view.login.LoginActivity
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_vs_com.*
import javax.inject.Inject

class VsComActivity : DaggerAppCompatActivity(), Design, VsComView {
    private lateinit var ivPlayerChoices: List<ImageView>
    private lateinit var ivComChoices: List<ImageView>
    private lateinit var userId: String
    private lateinit var token: String
    private var logResult = ""
    private var isFav = false

    //Dagger
    @Inject
    lateinit var presenter: VsComPresenter

    @Inject
    lateinit var preferenceHelper: PreferenceHelper

    //Koin
//    private val preferenceHelper by inject<PreferenceHelper>()
//    private val presenter by inject<VsComPresenter>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vs_com)

        setSupportActionBar(findViewById(R.id.my_toolbar))
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = ""

        headerBuilder(this, 35.0f)
        presenter.setView(this)
        val username = preferenceHelper.username
        userId = preferenceHelper.userId!!
        token = preferenceHelper.token!!
        val enemy = "CPU"
        player1.text = username
        presenter.setPlayer1(username!!)
        presenter.setPlayer2(enemy)

        ivPlayerChoices = listOf(guntingKiri, batuKiri, kertasKiri)
        ivComChoices = listOf(guntingKanan, batuKanan, kertasKanan)

        val clickedChoice = mutableListOf<ImageView>()

        for (item in ivPlayerChoices) {
            item.setOnClickListener {
                var choice = item.contentDescription.toString().toInt()
                val plyChoice = presenter.listofChoice[choice]
                presenter.setPlayerChoice(1, choice)
                clickedChoice.add(item)
                item.setBackgroundResource(R.drawable.bg_choice_picked)
                logResult = logResult.plus("P1: $plyChoice ")
                Log.i(VS_CPU_ACTIVITY, "P1: $plyChoice")

                presenter.setComputerChoice()
                choice = presenter.getComputerChoice()
                val cpuChoice = presenter.listofChoice[presenter.getComputerChoice()]
                clickedChoice.add(ivComChoices[choice])
                logResult = logResult.plus("CPU: $cpuChoice")
                Log.i(VS_CPU_ACTIVITY, "CPU: $cpuChoice")

                presenter.getWinner(token)
                Log.i(VS_CPU_ACTIVITY, logResult)
            }
        }

        btnReset.setOnClickListener {
            reset(hasil, ivPlayerChoices, ivComChoices, clickedChoice, btnFav)
            logResult = ""
            clickedChoice.removeAll(clickedChoice)
            isFav = false
        }

        btnFav.setOnClickListener {
            isFav = if (!isFav) {
                btnFav.setBackgroundResource(R.drawable.ic_save_active)
                presenter.saveHistory(userId)
                true
            } else {
                btnFav.setBackgroundResource(R.drawable.ic_save)
                presenter.deleteLatestHistory(userId)
                false
            }

        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun setWinner(result: String) {
        logResult = logResult.plus("Hasil: ${result.replace(Regex("\n"), " ")}")

        hasil.apply {
            text = result
            setTextColor(resources.getColor(R.color.textColorWhite))
            if (result == "DRAW") {
                textSize = 40.0f
                setBackgroundColor(resources.getColor(R.color.bgDrawBlue))
            } else {
                textSize = 30.0f
                setBackgroundColor(resources.getColor(R.color.bgWinGreen))
            }
        }
        setIsClickable(ivPlayerChoices, false)
        setImgAlpha(ivComChoices, 40)
        btnFav.visibility = View.VISIBLE
    }

    override fun showError(error: String) {
        Toast.makeText(this, error, Toast.LENGTH_LONG).show()
    }

    override fun setComChoiceBackground(choice: Int) {
        ivComChoices[choice].setBackgroundResource(R.drawable.bg_choice_picked)
    }

    override fun tokenUnauthorized() {
        preferenceHelper.token = ""
        preferenceHelper.username = ""
        preferenceHelper.userId = ""
        val intent = Intent(this, LoginActivity::class.java).apply {
            putExtra("resultRegister", "Session anda telah berakhir, silahkan login kembali")
        }
        startActivity(intent)
        finish()
    }
}
