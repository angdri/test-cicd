package com.example.janken31.view.menu.fragment.profile

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.janken31.R
import com.example.janken31.presenter.ProfilePresenter
import com.example.janken31.utils.Design
import com.example.janken31.utils.PROFILE_FRAGMENT
import com.example.janken31.utils.PROFILE_PRESENTER
import com.example.janken31.utils.PreferenceHelper
import com.example.janken31.view.login.LoginActivity
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.dialog_confirm.view.*
import kotlinx.android.synthetic.main.dialog_edit_profile.view.*
import kotlinx.android.synthetic.main.fragment_profile.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.File
import java.net.URL
import javax.inject.Inject


class ProfileFragment : DaggerFragment(), Design, ProfileView {

    private lateinit var email: String
    private lateinit var username: String
    private lateinit var token: String
    private var fileURL: URL? = null
    private var fileUri: Uri? = null
    private lateinit var menuItemEdit:MenuItem

    //Dagger
    @Inject
    lateinit var preferenceHelper: PreferenceHelper

    @Inject
    lateinit var presenter: ProfilePresenter

    //Koin
//    private val preferenceHelper by inject<PreferenceHelper>()
//    private val presenter by inject<ProfilePresenter>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as? AppCompatActivity)?.setSupportActionBar(activity?.findViewById(R.id.my_toolbar))
        (activity as? AppCompatActivity)?.supportActionBar?.title = ""

        roundImage(requireActivity().baseContext, ivProfilePicture, R.drawable.profilepict)
        roundImage(requireActivity().baseContext, ivPickProfile, R.drawable.pick_profile_pict)
        presenter.setView(this)

        token = preferenceHelper.token!!
        getData(token)

        btnSimpan.setOnClickListener {
            showConfirmDialog()
        }

        ivPickProfile.setOnClickListener {
            CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .start(requireContext(), this)
        }

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_app_bar_profile, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.btn_edit -> {
                showEditMenu()
                menuItemEdit = item
                menuItemEdit.isVisible = false
                (activity as? AppCompatActivity)?.supportActionBar?.setDisplayHomeAsUpEnabled(true)
                true
            }
            android.R.id.home -> {
                Log.d("test","teeee")
                hideEditMenu()
                menuItemEdit.isVisible = true
                (activity as? AppCompatActivity)?.supportActionBar?.setDisplayHomeAsUpEnabled(false)
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }

    override fun updateData(username: String, email: String) {
        val usernameRequestBody = username.toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val emailRequestBody = email.toRequestBody("multipart/form-data".toMediaTypeOrNull())
        var filePart:MultipartBody.Part? = null
        if(fileUri!=null) {
            val file: File? = File(fileUri?.path!!)
            val requestFile = file!!.asRequestBody("multipart/form-data".toMediaTypeOrNull())
            filePart = MultipartBody.Part.createFormData("photo", file.name, requestFile)
        }
        presenter.editData(token, usernameRequestBody, emailRequestBody, filePart)
    }

    private fun showEditMenu(){
        btnSimpan.visibility = View.VISIBLE
        ivPickProfile.visibility = View.VISIBLE
        tvProfileEmailForm.isClickable = true
        tvProfileUsernameForm.isClickable = true
        setOnClickListener(tvProfileEmailTitle, tvProfileEmailForm, true)
        setOnClickListener(tvProfileUsernameTitle, tvProfileUsernameForm, true)
    }

    private fun hideEditMenu(){
        btnSimpan.visibility = View.GONE
        ivPickProfile.visibility = View.GONE
        tvProfileEmailForm.isClickable = false
        tvProfileUsernameForm.isClickable = false
        setOnClickListener(tvProfileEmailTitle, tvProfileEmailForm, false)
        setOnClickListener(tvProfileUsernameTitle, tvProfileUsernameForm, false)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)
            if (resultCode == Activity.RESULT_OK) {
                fileUri = result.uri
                Glide.with(activity?.baseContext!!)
                    .load(fileUri)
                    .apply(RequestOptions().circleCrop())
                    .into(ivProfilePicture)
                Log.d("UriPath", fileUri?.path.toString())
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                val error = result.error
                Log.e(PROFILE_FRAGMENT, error.toString())
            }
        }
    }

    override fun setData(username: String, email: String) {
        this.email = email
        this.username = username
        preferenceHelper.username = username
        tvProfileEmailForm.text = email
        tvProfileUsernameForm.text = username
    }

    override fun setPhoto(url: String) {
        fileURL = URL(url)
        Glide.with(activity?.baseContext!!)
            .load(url)
            .fitCenter()
            .circleCrop()
            .into(ivProfilePicture)
    }

    override fun showEditDialog(title: String, data: String) {
        this.let {
            val builder = AlertDialog.Builder(activity)
            val inflateView =
                this.layoutInflater.inflate(R.layout.dialog_edit_profile, llProfile, false)
            builder.setView(inflateView)
            inflateView.tvEditProfileTitle.text = title
            inflateView.etEditProfileForm.setText(data)
            val dialog = builder.create()
            dialog.show()

            inflateView.btnCancelUpdateProfile.setOnClickListener {
                dialog.cancel()
            }

            inflateView.btnUpdateProfile.setOnClickListener {
                if (title == "Username") {
                    tvProfileUsernameForm.text = inflateView.etEditProfileForm.text.toString()
                } else {
                    tvProfileEmailForm.text = inflateView.etEditProfileForm.text.toString()
                }
                dialog.cancel()
            }
        }
    }

    override fun setOnClickListener(viewTitle: TextView, viewForm: TextView, set: Boolean) {
        if (set) {
            viewForm.setOnClickListener {
                showEditDialog(
                    viewTitle.text.toString(),
                    viewForm.text.toString()
                )
            }
        } else {
            viewForm.setOnClickListener {}
        }
    }

    override fun showMessage(msg: String) {
        Toast.makeText(activity, msg, Toast.LENGTH_LONG).show()
    }

    override fun showLoading(visibility: Int) {
        pbProfile.visibility = visibility
        if (visibility == View.GONE) {
            contentProfileFragment.visibility = View.VISIBLE
        } else {
            contentProfileFragment.visibility = View.GONE
        }
    }

    override fun getData(token: String) {
        presenter.getData(token)
    }

    override fun showConfirmDialog() {
        this.let {

            val builder = AlertDialog.Builder(activity)
            val dialogView =
                this.layoutInflater.inflate(R.layout.dialog_confirm, null, false)
            builder.setView(dialogView)
            dialogView.tvConfirmDialog.text = "Apakah data yang anda masukkan sudah fix untuk diupdate?"
            dialogView.btnConfirmDialog.text = "Update"
            val dialog = builder.create()

            dialog.show()

            dialogView.btnCancelConfirmDialog.setOnClickListener {
                dialog.cancel()
            }

            dialogView.btnConfirmDialog.setOnClickListener {
                hideEditMenu()
                (activity as? AppCompatActivity)?.supportActionBar?.setDisplayHomeAsUpEnabled(false)
                menuItemEdit.isVisible = true
                updateData(
                    tvProfileUsernameForm.text.toString(),
                    tvProfileEmailForm.text.toString()
                )
                dialog.cancel()
            }
        }
    }

    override fun tokenUnauthorized() {
        preferenceHelper.token = ""
        preferenceHelper.username = ""
        preferenceHelper.userId = ""
        val intent = Intent(activity, LoginActivity::class.java).apply {
            putExtra("resultRegister", "Session anda telah berakhir, silahkan login kembali")
        }
        startActivity(intent)
        requireActivity().finish()
    }
}
