package com.example.janken31.view.menu.fragment.history

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.janken31.R
import com.example.janken31.model.api.battle.response.GetBattleData
import kotlinx.android.synthetic.main.item_history.view.*

class HistoryAdapter : RecyclerView.Adapter<HistoryAdapter.ViewHolder>(){
    private var listHistory = mutableListOf<GetBattleData>()

    inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view){
        fun bind(history: GetBattleData){
            view.tvHistoryTanggalForm.text = history.createdAt!!.substring(0,10).replace("-","/")
            view.tvHistoryHasilForm.text = history.message
            view.tvHistoryModeForm.text = if(history.mode == "Singleplayer") "SinglePlayer" else "MultiPlayer"
            view.btnDeleteHistory.visibility = View.GONE
        }
    }

    fun addData(listHistory: List<GetBattleData>){
        this.listHistory.clear()
        this.listHistory.addAll(listHistory)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_history, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = listHistory.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(listHistory[position])
    }

}