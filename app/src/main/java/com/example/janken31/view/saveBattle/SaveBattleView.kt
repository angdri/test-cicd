package com.example.janken31.view.saveBattle

import com.example.janken31.model.room.History

interface SaveBattleView {
    fun historyIsEmpty()
    fun showHistory(result : MutableList<History>)
    fun showDeleteDialog(history: History, userId: String)
}