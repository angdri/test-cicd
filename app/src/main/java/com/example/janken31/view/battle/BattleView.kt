package com.example.janken31.view.battle

import android.graphics.Color
import android.widget.ImageView
import android.widget.TextView
import com.example.janken31.R

interface BattleView {
    fun reset(tvHasil: TextView, ivPlayer1Choice: List<ImageView>, ivPlayer2Choice:List<ImageView>, ivClickedChoice: List<ImageView>, btnFav: ImageView){
        setBackgroundColor(ivClickedChoice, R.color.background)
        setIsClickable(ivPlayer1Choice, true)
        setImgAlpha(ivPlayer2Choice, 255)
        tvHasil.apply {
            text = resources.getText(R.string.vs)
            textSize = 80.0f
            setTextColor(resources.getColor(R.color.textColorRed))
            setBackgroundColor(Color.TRANSPARENT)
        }
        btnFav.setBackgroundResource(R.drawable.ic_save)
    }

    fun setBackgroundColor(ivList : List<ImageView>, color: Int){
        for(item in ivList){
            item.setBackgroundResource(color)
        }
    }

    fun setImgAlpha(ivList: List<ImageView>, alpha: Int){
        for(item in ivList){
            item.imageAlpha = alpha
        }
    }

    fun setIsClickable(ivList: List<ImageView>, bool: Boolean){
        for(item in ivList){
            item.isClickable = bool
        }

        if(!bool){
            setImgAlpha(ivList, 40)
        }else{
            setImgAlpha(ivList, 255)
        }
    }
}