package com.example.janken31.view.register

interface RegisterView {
    fun registerResult(result: Boolean)
    fun showMessage(msg: String)
    fun reset()
    fun showLoading(visibility : Int)
}