package com.example.janken31.view.landingPage

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.example.janken31.view.landingPage.fragment.LandingPageFirstFragment
import com.example.janken31.view.landingPage.fragment.LandingPageSecondFragment
import com.example.janken31.view.landingPage.fragment.LandingPageThirdFragment

class AdapterViewPager(fm: FragmentManager) :
    FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    private val dataFragment = mutableListOf<Fragment>().apply {
        add(LandingPageFirstFragment())
        add(LandingPageSecondFragment())
        add(LandingPageThirdFragment())
    }

    override fun getItem(position: Int): Fragment {
        return dataFragment[position]
    }

    override fun getCount(): Int {
        return dataFragment.size
    }
}