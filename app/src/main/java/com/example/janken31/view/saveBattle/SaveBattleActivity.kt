package com.example.janken31.view.saveBattle

import android.app.AlertDialog
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.janken31.R
import com.example.janken31.model.room.History
import com.example.janken31.presenter.SaveBattlePresenter
import com.example.janken31.utils.PreferenceHelper
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_save_battle.*
import kotlinx.android.synthetic.main.dialog_confirm.view.*
import javax.inject.Inject


class SaveBattleActivity : DaggerAppCompatActivity(), SaveBattleView {

    private lateinit var adapter: SaveBattleAdapter

    //Dagger
    @Inject
    lateinit var presenter: SaveBattlePresenter

    @Inject
    lateinit var preferenceHelper: PreferenceHelper

//    //Koin
//    private val preferenceHelper by inject<PreferenceHelper>()
//    private val presenter by inject<SaveBattlePresenter>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_save_battle)
        setSupportActionBar(findViewById(R.id.my_toolbar))
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = ""

        presenter.setView(this)
        val userId = preferenceHelper.userId!!
        adapter = SaveBattleAdapter()

        adapter.setOnClickListener(object : SaveBattleAdapter.OnClickListenerHistory {
            override fun deleteHistory(history: History, position: Int) {
                showDeleteDialog(history, userId)
            }
        })

        presenter.checkHistoryById(userId)

        rvHistory.layoutManager = LinearLayoutManager(baseContext)
        rvHistory.adapter = adapter

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }


    override fun historyIsEmpty() {
        rvHistory.visibility = View.INVISIBLE
        tvEmptyHistory.visibility = View.VISIBLE
    }

    override fun showHistory(result: MutableList<History>) {
        rvHistory.visibility = View.VISIBLE
        tvEmptyHistory.visibility = View.GONE
        adapter.addData(result)
    }

    override fun showDeleteDialog(history: History, userId: String) {
        this.let {
            val builder = AlertDialog.Builder(this)
            val dialogView =
                this.layoutInflater.inflate(R.layout.dialog_confirm, null, false)
            builder.setView(dialogView)
            val dialog = builder.create()

            dialog.show()

            dialogView.btnCancelConfirmDialog.setOnClickListener {
                dialog.cancel()
            }

            dialogView.btnConfirmDialog.setOnClickListener {
                presenter.deleteHistory(history, userId)
                dialog.cancel()
            }

        }
    }
}
