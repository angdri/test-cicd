package com.example.janken31.view.menu

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.example.janken31.R
import com.example.janken31.utils.PreferenceHelper
import com.example.janken31.view.login.LoginActivity
import com.example.janken31.view.menu.fragment.battle.BattleFragment
import com.example.janken31.view.menu.fragment.history.HistoryFragment
import com.example.janken31.view.menu.fragment.profile.ProfileFragment
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_menu.*
import javax.inject.Inject


class MenuActivity : DaggerAppCompatActivity(), MenuView {

    //Dagger
    @Inject
    lateinit var preferenceHelper: PreferenceHelper

    //Koin
//    private val preferenceHelper by inject<PreferenceHelper>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)

        navigateFragment(BattleFragment())
        supportActionBar?.title = "Home"
        bottomNavigation.itemIconTintList = null
        bottomNavigation.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.menu_battle -> {
                    navigateFragment(BattleFragment())
                }
                R.id.menu_history -> {
                    navigateFragment(HistoryFragment())
                }
                R.id.menu_profile -> {
                    navigateFragment(ProfileFragment())
                    supportActionBar?.title = "Profile"
                }
            }
            true
        }

    }

//    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
//        val inflater = menuInflater
//        inflater.inflate(R.menu.menu_app_bar, menu)
//        return super.onCreateOptionsMenu(menu)
//    }
//
//    override fun onOptionsItemSelected(item: MenuItem): Boolean {
//       return when (item.itemId) {
//            R.id.app_bar_save_battle -> {
//                startActivity(Intent(this, SaveBattleActivity::class.java))
//                true
//            }
//            R.id.app_bar_keluar_aplikasi -> {
//                finishAffinity()
//                true
//            }
//            R.id.app_bar_logout -> {
//                logout()
//                true
//            }
//            else -> {
//                super.onOptionsItemSelected(item)
//            }
//        }
//    }

    private fun navigateFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.flMenuActivity, fragment)
            commit()
        }
    }

    override fun logout() {
        preferenceHelper.token = ""
        preferenceHelper.username = ""
        preferenceHelper.userId = ""
        val intent = Intent(this, LoginActivity::class.java).apply {
            putExtra("resultRegister", "Logout berhasil!")
        }
        startActivity(intent)
        finish()
    }
}
