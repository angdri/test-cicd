package com.example.janken31.view.login

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.View
import com.example.janken31.R
import com.example.janken31.model.api.user.auth.LoginAuth
import com.example.janken31.model.api.user.response.UserAuthResponse
import com.example.janken31.presenter.LoginPresenter
import com.example.janken31.utils.DataRegexValidation.Companion.isEmailValid
import com.example.janken31.utils.LOGIN_PAGE
import com.example.janken31.utils.PreferenceHelper
import com.example.janken31.view.menu.MenuActivity
import com.example.janken31.view.register.RegisterActivity
import com.google.android.material.snackbar.Snackbar
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_login.*
import javax.inject.Inject

class LoginActivity : DaggerAppCompatActivity(), LoginView {

    //Dagger
    @Inject
    lateinit var presenter: LoginPresenter

    @Inject
    lateinit var preferenceHelper: PreferenceHelper

    //Koin
//    private val preferenceHelper by inject<PreferenceHelper>()
//    private val presenter by inject<LoginPresenter>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        presenter.setView(this)

        val etEmail = etLoginEmail
        val etPassword = etLoginPassword
        val resultRegister = intent.getStringExtra("resultRegister")

        if (resultRegister != null) {
            showMessage(resultRegister)
        }

        tvRegister.setOnClickListener {
            startActivity(Intent(baseContext, RegisterActivity::class.java))
            Log.d(LOGIN_PAGE, "go to RegisterActivity")
        }

        btnReset.setOnClickListener {
            reset()
        }

        btnLogin.setOnClickListener {
            val email = etEmail.text.toString()
            val password = etPassword.text.toString()

            if (email == "" && password == "") {
                showMessage("Email dan password wajib diisi!")
            } else if (!isEmailValid(email)) {
                showMessage("Format email salah!")
            } else {
                val data = LoginAuth(
                    email,
                    password
                )
                presenter.loginUser(data)
            }
        }

    }

    @SuppressLint("DefaultLocale")
    override fun loginResult(status: Boolean, user: UserAuthResponse) {
        if (status) {
            preferenceHelper.token = "Bearer " + user.userAuthData!!.token
            preferenceHelper.username = user.userAuthData.username!!.capitalize()
            preferenceHelper.userId = user.userAuthData.id!!

            val intent = Intent(this, MenuActivity::class.java)
            startActivity(intent)
            finish()
            Log.d(LOGIN_PAGE, "Go to MenuActivity")
        }
    }

    override fun reset() {
        etLoginPassword.setText("")
        etLoginEmail.setText("")
    }

    override fun showMessage(data: String) {
        val snackBar = Snackbar.make(clLoginActivity, data, Snackbar.LENGTH_LONG).apply {
            setTextColor(Color.RED)
            view.setBackgroundColor(Color.WHITE)
            view.textAlignment = View.TEXT_ALIGNMENT_CENTER
        }
        snackBar.show()
    }

    override fun showLoading(visibility: Int) {
        pbLogin.visibility = visibility
        if (visibility == View.GONE) {
            contentLoginActivity.visibility = View.VISIBLE
        } else {
            contentLoginActivity.visibility = View.GONE
        }
    }
}
