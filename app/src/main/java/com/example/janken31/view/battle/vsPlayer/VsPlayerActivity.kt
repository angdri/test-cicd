package com.example.janken31.view.battle.vsPlayer

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import com.example.janken31.R
import com.example.janken31.presenter.VsPlayerPresenter
import com.example.janken31.utils.Design
import com.example.janken31.utils.PreferenceHelper
import com.example.janken31.utils.VS_PLAYER_ACTIVITY
import com.example.janken31.view.login.LoginActivity
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_vs_player.*
import javax.inject.Inject

class VsPlayerActivity : DaggerAppCompatActivity(), Design, VsPlayerView {
    private lateinit var ivPlayer1Choice: List<ImageView>
    private lateinit var ivPlayer2Choice: List<ImageView>
    private lateinit var userId: String
    private lateinit var token: String
    private var logResult = ""
    private var isFav = false

    //Dagger
    @Inject
    lateinit var presenter: VsPlayerPresenter

    @Inject
    lateinit var preferenceHelper: PreferenceHelper

    //Koin
//    private val preferenceHelper by inject<PreferenceHelper>()
//    private val presenter by inject<VsPlayerPresenter>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vs_player)

        setSupportActionBar(findViewById(R.id.my_toolbar))
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = ""

        headerBuilder(this, 35.0f)
        presenter.setView(this)
        val username = preferenceHelper.username!!
        userId = preferenceHelper.userId!!
        token = preferenceHelper.token!!
        player1.text = username
        val enemy = "Player 2"
        presenter.setPlayer1(username)
        presenter.setPlayer2(enemy)

        ivPlayer1Choice = listOf(guntingKiri, batuKiri, kertasKiri)
        ivPlayer2Choice = listOf(guntingKanan, batuKanan, kertasKanan)

        val clickedChoice = mutableListOf<ImageView>()
        setIsClickable(ivPlayer2Choice, false)

        for (item in ivPlayer1Choice) {
            item.setOnClickListener {
                val choice = item.contentDescription.toString().toInt()
                val ply1Choice = presenter.listOfChoice[choice]

                presenter.setPlayerChoice(1, choice)
                clickedChoice.add(item)
                logResult = logResult.plus("P1: $ply1Choice ")
                Log.i(VS_PLAYER_ACTIVITY, "P1: $ply1Choice")
                setIsClickable(ivPlayer1Choice, false)
                setIsClickable(ivPlayer2Choice, true)
            }
        }

        for (item in ivPlayer2Choice) {
            item.setOnClickListener {
                val choice = item.contentDescription.toString().toInt()
                val ply2Choice = presenter.listOfChoice[choice]

                presenter.setPlayerChoice(2, choice)
                clickedChoice.add(item)
                clickedChoice.forEach {
                    it.setBackgroundResource(R.drawable.bg_choice_picked)
                }
                logResult = logResult.plus("P2: $ply2Choice ")
                Log.i(VS_PLAYER_ACTIVITY, "P2: $ply2Choice")
                setIsClickable(ivPlayer2Choice, false)

                presenter.getWinner(token)
                Log.i(VS_PLAYER_ACTIVITY, logResult)
            }
        }

        btnReset.setOnClickListener {
            reset(hasil, ivPlayer1Choice, ivPlayer2Choice, clickedChoice, btnFav)
            logResult = ""
            clickedChoice.removeAll(clickedChoice)
            isFav = false
        }

        btnFav.setOnClickListener {
            isFav = if (!isFav) {
                btnFav.setBackgroundResource(R.drawable.ic_save_active)
                presenter.saveHistory(userId)
                true
            } else {
                btnFav.setBackgroundResource(R.drawable.ic_save)
                presenter.deleteLatestHistory(userId)
                false
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun setWinner(result: String) {
        logResult = logResult.plus("Hasil: ${result.replace(Regex("\n"), " ")}")

        hasil.apply {
            text = result
            setTextColor(resources.getColor(R.color.textColorWhite))
            if (result == "DRAW") {
                textSize = 40.0f
                setBackgroundColor(resources.getColor(R.color.bgDrawBlue))
            } else {
                textSize = 30.0f
                setBackgroundColor(resources.getColor(R.color.bgWinGreen))
            }
        }
        setIsClickable(ivPlayer1Choice, false)
        setImgAlpha(ivPlayer2Choice, 40)
        btnFav.visibility = View.VISIBLE
    }

    override fun showError(error: String) {
        Toast.makeText(this, error, Toast.LENGTH_LONG).show()
    }

    override fun tokenUnauthorized() {
        preferenceHelper.token = ""
        preferenceHelper.username = ""
        preferenceHelper.userId = ""
        val intent = Intent(this, LoginActivity::class.java).apply {
            putExtra("resultRegister", "Session anda telah berakhir, silahkan login kembali")
        }
        startActivity(intent)
        finish()
    }
}
