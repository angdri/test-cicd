package com.example.janken31.view

import android.content.Intent
import android.media.MediaPlayer
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.example.janken31.R
import com.example.janken31.presenter.MainPresenter
import com.example.janken31.utils.Design
import com.example.janken31.utils.MAIN_ACTIVITY
import com.example.janken31.utils.PreferenceHelper
import com.example.janken31.view.landingPage.LandingPageActivity
import com.example.janken31.view.menu.MenuActivity
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

class MainActivity : DaggerAppCompatActivity(), Design, MainView {
    //Dagger
    @Inject
    lateinit var presenter: MainPresenter

    //Koin
//    private val presenter by inject<MainPresenter>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //headerBuilder
        headerBuilder(this, 60.0f)
        presenter.setView(this)
        //get resource for audio, create mediaPlayer, start audio
        val res = resources.getIdentifier("press_start", "raw", packageName)
        val mediaPlayer = MediaPlayer.create(this, res)
        mediaPlayer.start()

        val preferenceHelper = PreferenceHelper(this)
        val token = preferenceHelper.token
        presenter.isTokenValid(token!!)

    }

    override fun showMessage(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show()
    }

    override fun isTokenValid(status: Boolean) {
        if(!status){
            startActivity(Intent(this, LandingPageActivity::class.java))
            finish()
            Log.d(MAIN_ACTIVITY, "Go to LandingPage")
        }else{
            startActivity(Intent(this, MenuActivity::class.java))
            finish()
            Log.d(MAIN_ACTIVITY, "Go to MenuActivity")
        }
    }
}
