package com.example.janken31.view.landingPage.fragment

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide

import com.example.janken31.R
import com.example.janken31.view.landingPage.LandingPageView
import kotlinx.android.synthetic.main.fragment_landing_page_third.*

class LandingPageThirdFragment : Fragment() {
    private lateinit var landingPageView: LandingPageView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_landing_page_third, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Glide.with(activity?.baseContext!!)
            .load("https://raw.githubusercontent.com/lylluc-backup/resourcesJanken/master/profile_pict.png")
            .fitCenter()
            .into(imgProfilePicture)
        landingPageView.showNextButton(true)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try{
            landingPageView = context as LandingPageView
        }catch (e: Exception){
            e.printStackTrace()
        }
    }
}
