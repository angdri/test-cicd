package com.example.janken31.di

import com.example.janken31.view.MainActivity
import com.example.janken31.view.battle.vsCom.VsComActivity
import com.example.janken31.view.battle.vsPlayer.VsPlayerActivity
import com.example.janken31.view.login.LoginActivity
import com.example.janken31.view.menu.MenuActivity
import com.example.janken31.view.register.RegisterActivity
import com.example.janken31.view.saveBattle.SaveBattleActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule{
    @ContributesAndroidInjector
    abstract fun mainActivity(): MainActivity

    @ContributesAndroidInjector
    abstract fun vsComActivity(): VsComActivity

    @ContributesAndroidInjector
    abstract fun vsPlayerActivity(): VsPlayerActivity

    @ContributesAndroidInjector
    abstract fun loginActivity(): LoginActivity

    @ContributesAndroidInjector
    abstract fun menuActivity(): MenuActivity

    @ContributesAndroidInjector
    abstract fun registerActivity(): RegisterActivity

    @ContributesAndroidInjector
    abstract fun saveBattleActivity(): SaveBattleActivity

}