package com.example.janken31.di

import com.example.janken31.presenter.*
import com.example.janken31.repository.HistoryRepository
import com.example.janken31.utils.PreferenceHelper
import org.koin.dsl.module

val presenterModule = module {
    factory { HistoryPresenter(get()) }
    factory { LoginPresenter(get()) }
    factory { MainPresenter(get()) }
    factory { ProfilePresenter(get())}
    factory { RegisterPresenter(get()) }
    factory { SaveBattlePresenter(get()) }
    factory { VsPlayerPresenter(get(), get()) }
    factory { VsComPresenter(get(),get()) }
    factory { PreferenceHelper(get()) }
    factory { HistoryRepository(get()) }
}