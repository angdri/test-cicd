package com.example.janken31.di

import com.example.janken31.view.menu.fragment.history.HistoryFragment
import com.example.janken31.view.menu.fragment.profile.ProfileFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentModule {
    @ContributesAndroidInjector
    abstract fun historyFragment(): HistoryFragment

    @ContributesAndroidInjector
    abstract fun profileFragment(): ProfileFragment
}