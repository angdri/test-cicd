package com.example.janken31.di

import com.example.janken31.api.ApiService
import com.example.janken31.db.AppDatabase
import com.example.janken31.presenter.*
import com.example.janken31.repository.HistoryRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class PresenterModule{

    @Provides
    fun historyRepository(database: AppDatabase) : HistoryRepository = HistoryRepository(database)

    @Singleton
    @Provides
    fun mainPresenter(apiService:ApiService) = MainPresenter(apiService)

    @Singleton
    @Provides
    fun historyPresenter(apiService:ApiService) = HistoryPresenter(apiService)

    @Singleton
    @Provides
    fun loginPresenter(apiService:ApiService) = LoginPresenter(apiService)

    @Singleton
    @Provides
    fun profilePresenter(apiService:ApiService) = ProfilePresenter(apiService)

    @Singleton
    @Provides
    fun registerPresenter(apiService:ApiService) = RegisterPresenter(apiService)

    @Singleton
    @Provides
    fun saveBattlePresenter(repository: HistoryRepository) = SaveBattlePresenter(repository)

    @Singleton
    @Provides
    fun vsComPresenter(apiService:ApiService, repository: HistoryRepository) = VsComPresenter(repository, apiService)

    @Singleton
    @Provides
    fun vsPlayerPresenter(apiService: ApiService, repository: HistoryRepository) = VsPlayerPresenter(repository, apiService)
}
