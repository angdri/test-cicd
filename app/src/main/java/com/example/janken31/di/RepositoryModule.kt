package com.example.janken31.di

import android.app.Application
import android.content.Context
import androidx.room.Room
import com.example.janken31.api.ApiIdentifier
import com.example.janken31.api.ApiService
import com.example.janken31.db.AppDatabase
import com.example.janken31.utils.PreferenceHelper
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule {
    @Singleton
    @Provides
    fun apiService(): ApiService = ApiIdentifier.create()


    @Singleton
    @Provides
    fun prefHelper(context: Context) : PreferenceHelper = PreferenceHelper(context)

    @Singleton
    @Provides
    fun dbRoom(context: Context): AppDatabase {
        return Room
            .databaseBuilder(context, AppDatabase::class.java, "Janken3.1")
            .build()
    }

    @Singleton
    @Provides
    fun ctx(application: Application) : Context = application.applicationContext

}