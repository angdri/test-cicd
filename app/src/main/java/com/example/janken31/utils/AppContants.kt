@file:Suppress("SpellCheckingInspection")

package com.example.janken31.utils

//tag Log
const val MAIN_ACTIVITY = "MainActivity"
const val VS_PLAYER_ACTIVITY = "VSPlayerActivity"
const val VS_CPU_ACTIVITY = "VSCPUActivity"
const val LANDING_PAGE = "LandingPageActivity"
const val LOGIN_PAGE = "LoginActivity"
const val PROFILE_FRAGMENT = "ProfileFragment"
const val REGISTER_PRESENTER = "RegisterPresenter"
const val LOGIN_PRESENTER = "LoginPresenter"
const val PROFILE_PRESENTER = "ProfilePresenter"
const val HISTORY_PRESENTER = "HistoryPresenter"
const val VS_PLAYER_PRESENTER = "VSPlayerPresenter"
const val VS_CPU_PRESENTER = "VSCPUPresenter"

//Shared Preferences
const val APPS_NAME = "com.example.janken3"
const val USERNAME_KEY = "usernamePlayer"
const val USERID_KEY = "idPlayer"
const val USER_TOKEN_KEY = "tokenUser"

//GamePlay
const val GUNTING = 0
const val BATU = 1
const val KERTAS = 2

//API
const val BASE_URL_API = "https://binar-gdd-cc8.herokuapp.com/"