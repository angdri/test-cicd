package com.example.janken31.utils

import android.app.Activity
import android.content.Context
import android.graphics.BitmapFactory
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory
import com.example.janken31.R
import kotlinx.android.synthetic.main.activity_main.*

interface Design {
    fun headerBuilder(activity: Activity, textSize: Float) {
        val builder = SpannableStringBuilder()
        val str1 = SpannableString("Kertas\n")
        str1.setSpan(
            ForegroundColorSpan(activity.resources.getColor(R.color.textColorOrange)),
            0,
            str1.length,
            0
        )
        builder.append(str1)

        val str2 = SpannableString("Gunting\n")
        str2.setSpan(
            ForegroundColorSpan(activity.resources.getColor(R.color.textColorGreen)),
            0,
            str2.length,
            0
        )
        builder.append(str2)

        val str3 = SpannableString("Batu")
        str3.setSpan(
            ForegroundColorSpan(activity.resources.getColor(R.color.textColorPurple)),
            0,
            str3.length,
            0
        )
        builder.append(str3)

        activity.header!!.setText(builder, TextView.BufferType.SPANNABLE)
        activity.header!!.textSize = textSize
        activity.header!!.setLineSpacing(2.5f, 0.55f)

        val tf = ResourcesCompat.getFont(activity,
            R.font.comicbd
        )
        activity.header!!.typeface = tf
    }

    fun roundImage(context: Context, view:ImageView, imageId:Int){
        val img = BitmapFactory.decodeResource(context.resources, imageId)
        val round = RoundedBitmapDrawableFactory.create(context.resources, img)
        round.isCircular = true
        view.setImageDrawable(round)
    }
}