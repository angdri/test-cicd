package com.example.janken31.utils

import android.app.Activity
import android.app.Application
import android.content.Context
import androidx.multidex.MultiDex
import androidx.room.Room
import com.example.janken31.api.ApiIdentifier
import com.example.janken31.db.AppDatabase
import com.example.janken31.di.DaggerAppComponent
import com.example.janken31.di.presenterModule
import com.jakewharton.threetenabp.AndroidThreeTen
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.dsl.module
import javax.inject.Inject

class BaseApplication : Application(), HasActivityInjector {
    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    override fun activityInjector(): AndroidInjector<Activity> = dispatchingAndroidInjector

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    override fun onCreate() {
        super.onCreate()
        AndroidThreeTen.init(this)

        DaggerAppComponent.builder()
            .application(this)
            .build()
            .inject(this)

        startKoin {
            androidContext(this@BaseApplication)
            modules(module {
                single {
                    Room.databaseBuilder(applicationContext, AppDatabase::class.java, "Janken 3.1")
                        .build()
                }
                single { ApiIdentifier.create() }
            })
            modules(presenterModule)
        }
    }
}