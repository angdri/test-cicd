package com.example.janken31.utils

import android.content.Context
import android.content.SharedPreferences

class PreferenceHelper (context: Context) {
    private val sharedPreferences: SharedPreferences = context.getSharedPreferences(APPS_NAME, Context.MODE_PRIVATE)

    var username : String?
        get(){
            return sharedPreferences.getString(USERNAME_KEY,"")
        }

        set(value) {
            sharedPreferences.edit().apply{
                putString(USERNAME_KEY, value)
                apply()
            }
        }

    var userId : String?
        get() {
            return sharedPreferences.getString(USERID_KEY, "")
        }
        set(value) {
            sharedPreferences.edit().apply{
                putString(USERID_KEY, value)
                apply()
            }
        }

    var token : String?
    get(){
        return sharedPreferences.getString(USER_TOKEN_KEY, "")
    }

    set(value){
        sharedPreferences.edit().apply {
            putString(USER_TOKEN_KEY, value)
            apply()
        }
    }
}