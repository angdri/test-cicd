@file:Suppress("Annotator", "Annotator", "Annotator", "Annotator", "Annotator")

package com.example.janken31.utils

class DataRegexValidation {
    companion object {
        @JvmStatic
        val EMAIL_REGEX =
            "^[A-Za-z]([0-9A-Za-z._]*)([@]{1})([a-z\\-]{1,})((\\.([A-Za-z]{1,}))|(\\.([a-z]{1,})\\.([a-z]{1,})))"

        private const val USERNAME_REGEX = "^[A-Za-z0-9]*"

        //email (^) diawali [A-Za-z] dengan huruf besar/kecil, ([0-9a-zA-Z]*) mengandung 0 atau lebih kombinasi angka, huruf, titik dan underscore,
        // ([@]{1})diikuti dengan 1 @ yang diikuti dengan kombinasi huruf kecil dan -
        //yang diikuti dengan . dan kombinas huruf kecil atau . kombinasi huruf kecil . kobinasi huruf kecil ada
        fun isEmailValid(email: String): Boolean {
            return EMAIL_REGEX.toRegex().matches(email)
        }

        fun isUsernameValid(username: String): Boolean{
            return USERNAME_REGEX.toRegex().matches(username)
        }
    }
}