package com.example.janken31.model.api.battle.response


import com.fasterxml.jackson.annotation.JsonProperty

data class GetBattleData(
    @JsonProperty("createdAt")
    var createdAt: String? = null,
    @JsonProperty("_id")
    var id: String? = null,
    @JsonProperty("message")
    var message: String? = null,
    @JsonProperty("mode")
    var mode: String? = null,
    @JsonProperty("result")
    var result: String? = null,
    @JsonProperty("updatedAt")
    var updatedAt: String? = null
)