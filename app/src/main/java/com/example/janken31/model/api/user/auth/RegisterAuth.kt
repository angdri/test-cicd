package com.example.janken31.model.api.user.auth


import com.fasterxml.jackson.annotation.JsonProperty

data class RegisterAuth(
    @JsonProperty("email")
    val email: String,
    @JsonProperty("password")
    val password: String,
    @JsonProperty("username")
    val username: String
)