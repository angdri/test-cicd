package com.example.janken31.model.api.user.response


import com.fasterxml.jackson.annotation.JsonProperty

data class UserAuthResponse(
    @JsonProperty("data")
    val userAuthData: UserAuthData? = null,
    @JsonProperty("success")
    val success: Boolean? = null,
    @JsonProperty("errors")
    val errors: String? = null
)