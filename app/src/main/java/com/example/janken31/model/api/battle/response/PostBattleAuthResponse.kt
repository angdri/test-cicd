package com.example.janken31.model.api.battle.response


import com.fasterxml.jackson.annotation.JsonProperty

data class PostBattleAuthResponse(
    @JsonProperty("data")
    var postBattleData: PostBattleData? = null,
    @JsonProperty("success")
    var success: Boolean? = null
)