@file:Suppress("SpellCheckingInspection")

package com.example.janken31.model

import com.example.janken31.utils.BATU
import com.example.janken31.utils.GUNTING
import com.example.janken31.utils.KERTAS

class Battle {
    private lateinit var player1: Player
    private lateinit var player2: Player

    val listofChoice = listOf("Gunting","Batu","Kertas")

    fun setPlayer1(name: String){
        player1 = Player(name, null)
    }

    fun setPlayer2(name: String){
        player2 = Player(name, null)
    }

    fun setPlayer1Choice(choice:Int) {
        player1.inputPilihan(choice)
    }

    fun setPlayer2Choice(){
        player2.randomChoice()
    }

    fun setPlayer2Choice(choice: Int){
        player2.inputPilihan(choice)
    }

    fun getPlayer2Choice(): Int{
        return player2.getPilihan()!!
    }

    fun getPlayer2Name(): String{
        return player2.name
    }

    fun getResult():String{
        return if ((player1.getPilihan() == BATU && player2.getPilihan() == GUNTING) ||
            (player1.getPilihan() == KERTAS && player2.getPilihan() == BATU) ||
            (player1.getPilihan() == GUNTING && player2.getPilihan() == KERTAS)) {
            "${player1.name}\nMenang"
        } else if (player1.getPilihan() == player2.getPilihan()) {
            "DRAW"
        } else {
            "${player2.name}\nMenang"
        }
    }
}