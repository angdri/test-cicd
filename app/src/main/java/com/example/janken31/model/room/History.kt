package com.example.janken31.model.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "history_table")
data class History (
    @PrimaryKey(autoGenerate = true) val historyId: Long? = null,
    @ColumnInfo(name = "userId") val userId: String,
    @ColumnInfo(name = "dateBattle") val date: String,
    @ColumnInfo(name = "battleMode") val battleMode: String,
    @ColumnInfo(name = "result") val result: String
)