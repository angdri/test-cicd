package com.example.janken31.model.api.battle.auth


import com.fasterxml.jackson.annotation.JsonProperty

data class BattleAuth(
    @JsonProperty("mode")
    val mode: String,
    @JsonProperty("result")
    val result: String
)