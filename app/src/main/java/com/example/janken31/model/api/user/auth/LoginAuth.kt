package com.example.janken31.model.api.user.auth


import com.fasterxml.jackson.annotation.JsonProperty

data class LoginAuth(
    @JsonProperty("email")
    val email: String,
    @JsonProperty("password")
    val password: String
)