package com.example.janken31.model.api.battle.response


import com.fasterxml.jackson.annotation.JsonProperty

data class GetBattleAuthResponse(
    @JsonProperty("data")
    var `data`: List<GetBattleData>? = null,
    @JsonProperty("success")
    var success: Boolean? = null
)