package com.example.janken31.model

import com.example.janken31.utils.BATU
import com.example.janken31.utils.GUNTING
import com.example.janken31.utils.KERTAS

class Player(val name: String, private var pilihan: Int?) {

    fun inputPilihan(pilihanPly : Int){
        pilihan = pilihanPly
    }

    fun getPilihan(): Int?{
        return pilihan
    }

    fun randomChoice(){
        val pilihanRandom = listOf(GUNTING, BATU, KERTAS)
        pilihan = pilihanRandom.random()
    }
}