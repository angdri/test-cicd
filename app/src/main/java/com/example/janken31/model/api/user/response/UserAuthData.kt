package com.example.janken31.model.api.user.response


import com.fasterxml.jackson.annotation.JsonProperty

data class UserAuthData(
    @JsonProperty("email")
    val email: String? = null,
    @JsonProperty("_id")
    val id: String? = null,
    @JsonProperty("token")
    val token: String? = null,
    @JsonProperty("username")
    val username: String? = null,
    @JsonProperty("photo")
    val photo: String? = null
)