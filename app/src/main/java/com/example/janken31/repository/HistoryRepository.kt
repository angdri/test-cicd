package com.example.janken31.repository

import com.example.janken31.db.AppDatabase
import com.example.janken31.db.HistoryDao
import com.example.janken31.model.room.History

class HistoryRepository (database: AppDatabase) {
    private val historyDao: HistoryDao = database.historyDao()

    suspend fun insertHistory(history: History){
        historyDao.insertHistory(history)
    }

    suspend fun deleteHistory(history: History){
        historyDao.deleteHistory(history)
    }

    suspend fun getHistoryByUserId(userId: String, onGetHistory : (MutableList<History>) -> Unit){
        val result = historyDao.getHistoryByUserId(userId)
        onGetHistory(result)
    }

    suspend fun deleteUserLatestHistory(userId: String){
        historyDao.deleteUserLatestHistory(userId)
    }
}