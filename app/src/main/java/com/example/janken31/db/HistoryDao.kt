package com.example.janken31.db

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.example.janken31.model.room.History

@Dao
interface HistoryDao {
    @Query("SELECT * FROM history_table WHERE userId = :userId")
    suspend fun getHistoryByUserId(userId: String): MutableList<History>

    @Query("DELETE FROM history_table WHERE historyId = (SELECT MAX(historyId) FROM history_table WHERE userId = :userId)")
    suspend fun deleteUserLatestHistory(userId: String)

    @Insert(onConflict = REPLACE)
    suspend fun insertHistory(history: History)

    @Delete
    suspend fun deleteHistory(history: History)
}