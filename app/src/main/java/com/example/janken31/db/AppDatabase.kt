package com.example.janken31.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.janken31.model.room.History

@Database(entities = [History::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun historyDao() : HistoryDao
}