package com.example.janken31.api

import com.example.janken31.model.api.battle.auth.BattleAuth
import com.example.janken31.model.api.battle.response.GetBattleAuthResponse
import com.example.janken31.model.api.battle.response.PostBattleAuthResponse
import com.example.janken31.model.api.user.auth.LoginAuth
import com.example.janken31.model.api.user.auth.RegisterAuth
import com.example.janken31.model.api.user.response.UserAuthResponse
import io.reactivex.Flowable
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*


interface ApiService {

    @POST("api/v1/auth/register")
    fun authRegister(@Body registerAuth: RegisterAuth): Call<UserAuthResponse>

    @POST("api/v1/auth/login")
    fun authLogin(@Body loginAuth: LoginAuth): Call<UserAuthResponse>

    @GET("api/v1/auth/me")
    fun authMe(@Header("Authorization") token: String): Call<UserAuthResponse>

    @POST("api/v1/battle")
    fun postBattle(
        @Body battleAuth: BattleAuth,
        @Header("Authorization") token: String
    ): Call<PostBattleAuthResponse>

    @GET("api/v1/battle")
    fun getBattle(@Header("Authorization") token: String): Call<GetBattleAuthResponse>

    @GET("api/v1/users")
    fun getUsers(@Header("Authorization") token: String): Call<UserAuthResponse>

    @Multipart
    @PUT("api/v1/users")
    fun putUser(
        @Header("Authorization") token: String,
        @Part("username") username: RequestBody,
        @Part("email") email: RequestBody,
        @Part file: MultipartBody.Part?
    ): Call<UserAuthResponse>
}