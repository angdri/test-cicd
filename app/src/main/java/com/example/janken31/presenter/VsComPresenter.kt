package com.example.janken31.presenter

import android.util.Log
import com.example.janken31.api.ApiService
import com.example.janken31.model.Battle
import com.example.janken31.model.api.battle.auth.BattleAuth
import com.example.janken31.model.api.battle.response.PostBattleAuthResponse
import com.example.janken31.model.room.History
import com.example.janken31.repository.HistoryRepository
import com.example.janken31.utils.VS_CPU_PRESENTER
import com.example.janken31.view.battle.vsCom.VsComView
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.threeten.bp.LocalDate
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class VsComPresenter(
    private val repository: HistoryRepository,
    private val apiService: ApiService
) {
    private lateinit var view: VsComView

    fun setView(view: VsComView) {
        this.view = view
    }

    private val battle = Battle()
    private lateinit var history: History
    val listofChoice = battle.listofChoice

    fun setPlayerChoice(player: Int, choice: Int) {
        when (player) {
            1 -> battle.setPlayer1Choice(choice)
            else -> view.showError("Player tidak ada!")
        }
    }

    fun setPlayer1(username: String) {
        battle.setPlayer1(username)
    }

    fun setPlayer2(username: String) {
        battle.setPlayer2(username)
    }

    fun setComputerChoice() {
        battle.setPlayer2Choice()
        view.setComChoiceBackground(battle.getPlayer2Choice())
    }

    fun getComputerChoice(): Int {
        return battle.getPlayer2Choice()
    }

    fun getWinner(token: String) {
        val result = battle.getResult()
        val resultApi = when (battle.getResult().replace(Regex("\n"), " ")) {
            battle.getPlayer2Name() + " Menang" -> "Opponent Win"
            "DRAW" -> "Draw"
            else -> "Player Win"
        }
        val battleMode = "Singleplayer"
        val battleAuth = BattleAuth(battleMode, resultApi)
        apiService.postBattle(battleAuth, token).enqueue(object : Callback<PostBattleAuthResponse> {
            override fun onFailure(call: Call<PostBattleAuthResponse>, t: Throwable) {
                Log.d(VS_CPU_PRESENTER, t.message.toString())
            }

            override fun onResponse(
                call: Call<PostBattleAuthResponse>,
                response: Response<PostBattleAuthResponse>
            ) {
                when (response.code()) {
                    200 -> {

                    }
                    else -> {
                        view.tokenUnauthorized()
                    }
                }
            }

        })
        view.setWinner(result)
    }

    fun saveHistory(userId: String) {
        val result = when (battle.getResult().replace(Regex("\n"), " ")) {
            battle.getPlayer2Name() + " Menang" -> "Opponent Win"
            "DRAW" -> "Draw"
            else -> "Player Win"
        }
        val formatter = org.threeten.bp.format.DateTimeFormatter.ofPattern("yyyy/MM/dd")
        val date = LocalDate.now().format(formatter)
        val battleMode = "Singleplayer"
        history = History(
            null,
            userId,
            date,
            battleMode,
            result
        )
        GlobalScope.launch(Dispatchers.Main) {
            repository.insertHistory(history)

        }
    }

    fun deleteLatestHistory(userId: String) {
        GlobalScope.launch(Dispatchers.Main) {
            repository.deleteUserLatestHistory(userId)
        }
    }
}