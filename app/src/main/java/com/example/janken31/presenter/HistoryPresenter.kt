package com.example.janken31.presenter

import android.util.Log
import android.view.View
import com.example.janken31.api.ApiService
import com.example.janken31.model.api.battle.response.GetBattleAuthResponse
import com.example.janken31.model.api.battle.response.GetBattleData
import com.example.janken31.utils.HISTORY_PRESENTER
import com.example.janken31.view.menu.fragment.history.HistoryView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HistoryPresenter (private val apiService: ApiService) {
    private lateinit var view: HistoryView

    fun setView(view: HistoryView){
        this.view = view
    }

    fun getHistory(token: String){
        view.showLoading(View.VISIBLE)
        apiService.getBattle(token).enqueue(object : Callback<GetBattleAuthResponse>{
            override fun onFailure(call: Call<GetBattleAuthResponse>, t: Throwable) {
                Log.d(HISTORY_PRESENTER, t.message.toString())
                view.showMessage(t.message.toString())
                view.showLoading(View.GONE)
            }

            override fun onResponse(
                call: Call<GetBattleAuthResponse>,
                response: Response<GetBattleAuthResponse>
            ) {
                when(response.code()){
                    200 -> {
                        val listHistory = response.body()!!.data as MutableList<GetBattleData>
                        if(listHistory.size == 0){
                            view.historyIsEmpty()
                        }else{
                            view.showHistory(listHistory)
                        }
                        view.showLoading(View.GONE)
                    }
                    401 -> view.tokenUnauthorized()
                }

            }

        })
    }
}