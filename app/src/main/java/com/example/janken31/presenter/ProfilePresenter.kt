package com.example.janken31.presenter

import android.util.Log
import android.view.View
import com.example.janken31.api.ApiService
import com.example.janken31.model.api.user.response.UserAuthResponse
import com.example.janken31.utils.PROFILE_PRESENTER
import com.example.janken31.view.menu.fragment.profile.ProfileView
import com.fasterxml.jackson.databind.ObjectMapper
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProfilePresenter (private val apiService: ApiService) {
    private lateinit var view: ProfileView

    fun setView(view: ProfileView){
        this.view = view
    }
    fun getData(token: String) {
        view.showLoading(View.VISIBLE)
        apiService.getUsers(token).enqueue(object : Callback<UserAuthResponse> {
            override fun onFailure(call: Call<UserAuthResponse>, t: Throwable) {
                Log.d(PROFILE_PRESENTER, t.message.toString())
                view.showMessage(t.message.toString())
                view.showLoading(View.GONE)
            }

            override fun onResponse(
                call: Call<UserAuthResponse>,
                response: Response<UserAuthResponse>
            ) {
                val responseCode = response.code()
                if (responseCode == 200) {
                    val email = response.body()?.userAuthData!!.email
                    val username = response.body()?.userAuthData!!.username
                    val photoUrl = response.body()?.userAuthData!!.photo

                    if (photoUrl != null) {
                        view.setPhoto(photoUrl)
                    }
                    view.setData(username!!, email!!)
                } else {
                    view.tokenUnauthorized()
                }
                view.showLoading(View.GONE)
            }
        })
    }

    fun editData(token:String, username:RequestBody, email:RequestBody, file:MultipartBody.Part?){
        view.showLoading(View.VISIBLE)
        apiService.putUser(token,username,email,file).enqueue(object : Callback<UserAuthResponse>{
            override fun onFailure(call: Call<UserAuthResponse>, t: Throwable) {
                view.showMessage(t.message.toString())
                view.showLoading(View.GONE)
            }

            override fun onResponse(
                call: Call<UserAuthResponse>,
                response: Response<UserAuthResponse>
            ) {
                when(response.code()){
                    200 ->{
                        val username = response.body()?.userAuthData!!.username
                        val email = response.body()?.userAuthData!!.email
                        val photoUrl = response.body()?.userAuthData!!.photo!!
                        view.setData(username!!,email!!)
                        view.setPhoto(photoUrl)
                    }
                    422 -> {
//                        val responseError = response.errorBody()!!.string()
                        val responseError = ObjectMapper().readValue(
                            response.errorBody()?.charStream(),
                            UserAuthResponse::class.java
                        )
                        if(responseError.errors!!.contains("email_1 dup key")){
                            view.showMessage("Register gagal, email sudah terdaftar")
                        }else{
                            view.showMessage("Register gagal, username sudah terdafar")
                        }
                    }
                    401 -> {
                        view.tokenUnauthorized()
                    }
                }
                view.showLoading(View.GONE)
            }

        })
    }

}