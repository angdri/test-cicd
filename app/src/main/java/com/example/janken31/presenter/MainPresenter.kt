package com.example.janken31.presenter

import android.util.Log
import com.example.janken31.api.ApiService
import com.example.janken31.model.api.user.response.UserAuthResponse
import com.example.janken31.utils.MAIN_ACTIVITY
import com.example.janken31.view.MainView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainPresenter(private val apiService: ApiService) {
    private lateinit var view: MainView

    fun setView(view: MainView) {
        this.view = view
    }

    fun isTokenValid(data: String) {
        apiService.authMe(data).enqueue(object : Callback<UserAuthResponse> {
            override fun onFailure(call: Call<UserAuthResponse>, t: Throwable) {
                Log.d(MAIN_ACTIVITY, t.message.toString())
                view.showMessage(t.message.toString())
            }

            override fun onResponse(
                call: Call<UserAuthResponse>,
                response: Response<UserAuthResponse>
            ) {
                when (response.code()) {
                    200 -> view.isTokenValid(true)
                    else -> view.isTokenValid(false)
                }
            }

        })
    }
}