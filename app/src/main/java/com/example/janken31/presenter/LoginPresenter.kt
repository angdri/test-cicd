package com.example.janken31.presenter

import android.util.Log
import android.view.View
import com.example.janken31.api.ApiService
import com.example.janken31.model.api.user.auth.LoginAuth
import com.example.janken31.model.api.user.response.UserAuthResponse
import com.example.janken31.utils.LOGIN_PRESENTER
import com.example.janken31.view.login.LoginView
import com.fasterxml.jackson.databind.ObjectMapper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginPresenter(private val apiService: ApiService) {
    private lateinit var view: LoginView

    fun setView(view: LoginView) {
        this.view = view
    }

    fun loginUser(data: LoginAuth) {
        view.showLoading(View.VISIBLE)
        apiService.authLogin(data).enqueue(object : Callback<UserAuthResponse> {
            override fun onFailure(call: Call<UserAuthResponse>, t: Throwable) {
                Log.d(LOGIN_PRESENTER, t.message.toString())
                view.showMessage(t.message.toString())
                view.showLoading(View.GONE)
            }

            override fun onResponse(
                call: Call<UserAuthResponse>,
                response: Response<UserAuthResponse>
            ) {
                when (response.code()) {
                    200 -> view.loginResult(true, response.body() as UserAuthResponse)
                    401 -> {
                        val responseError = ObjectMapper().readValue(
                            response.errorBody()?.charStream(),
                            UserAuthResponse::class.java
                        ).errors
                        if (responseError == "Wrong password!") {
                            view.showMessage("Login gagal, password salah")
                        } else if (responseError == "Email doesn't exist!") {
                            view.showMessage("Email tidak ditemukan")
                        } else {
                            view.showMessage(responseError!!)
                        }
                    }
                }
                view.showLoading(View.GONE)
            }
        })
    }
}