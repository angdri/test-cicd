package com.example.janken31.presenter

import android.util.Log
import android.view.View
import com.example.janken31.api.ApiService
import com.example.janken31.model.api.user.auth.RegisterAuth
import com.example.janken31.model.api.user.response.UserAuthResponse
import com.example.janken31.utils.REGISTER_PRESENTER
import com.example.janken31.view.register.RegisterView
import com.fasterxml.jackson.databind.ObjectMapper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RegisterPresenter(private val apiService: ApiService) {
    private lateinit var view: RegisterView

    fun setView(view: RegisterView) {
        this.view = view
    }

    fun registerUser(data: RegisterAuth) {
        view.showLoading(View.VISIBLE)
        apiService.authRegister(data).enqueue(object : Callback<UserAuthResponse> {
            override fun onFailure(call: Call<UserAuthResponse>, t: Throwable) {
                Log.d(REGISTER_PRESENTER, t.message.toString())
                view.showMessage(t.message.toString())
                view.showLoading(View.GONE)
            }

            override fun onResponse(
                call: Call<UserAuthResponse>,
                response: Response<UserAuthResponse>
            ) {
                when (response.code()) {
                    201 -> view.registerResult(true)
                    422 -> {
//                        val responseError = response.errorBody()!!.string()
                        val responseError = ObjectMapper().readValue(
                            response.errorBody()?.charStream(),
                            UserAuthResponse::class.java
                        ).errors
                        if (responseError == "User validation failed: username: test@gmail.com should only contain alphanumeric characters") {
                            view.showMessage("Register gagal, username hanya boleh terdiri dari angka dan huruf")
                        } else if (responseError!!.contains("email_1 dup key")) {
                            view.showMessage("Register gagal, email sudah terdafar")
                        } else if (responseError.contains("username_1 dup key")) {
                            view.showMessage("Register gagal, username sudah terdafar")
                        } else {
                            view.showMessage(responseError)
                        }
                    }
                }
                view.showLoading(View.GONE)
            }

        })
    }
}