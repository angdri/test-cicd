package com.example.janken31.presenter

import com.example.janken31.model.room.History
import com.example.janken31.repository.HistoryRepository
import com.example.janken31.view.saveBattle.SaveBattleView
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class SaveBattlePresenter (private val repository: HistoryRepository){
    private lateinit var view: SaveBattleView

    fun setView(view: SaveBattleView){
        this.view = view
    }

    fun checkHistoryById(userId:String) = GlobalScope.launch(Dispatchers.Main){
        repository.getHistoryByUserId(userId, onGetHistory = {
            onGetHistory(it)
        })
    }

    fun deleteHistory(history: History, userId: String) = GlobalScope.launch(Dispatchers.Main){
        repository.deleteHistory(history)
        repository.getHistoryByUserId(userId, onGetHistory = {
            onGetHistory(it)
        })
    }

    private fun onGetHistory(listHistory:MutableList<History>){
        if(listHistory.size == 0){
            view.historyIsEmpty()
        }else{
            view.showHistory(listHistory)
        }
    }
}